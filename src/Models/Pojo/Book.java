package Models.Pojo;

/**
 *
 * @author Tejas
 */
public class Book {

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public float getPrice() {
        return price;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public Author getAuthor() {
        return author;
    }
    
    int id;
    String name;
    int author_id;
    float price;
    int total_pages;
    Author author;

    public Book() {
    }

    
}
