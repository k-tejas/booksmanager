/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Pojo;

/**
 *
 * @author Tejas
 */
public class Author {

    int id;
    String full_name;
    String location;
    int age;
    String details;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
    
    /*Overriding the toString method defines, what to show in Swing Componenets ex. jComboBox*/
    @Override
    public String toString(){
        return full_name;
    }

    public Author() {
    }
/* no need of this yet.
    public Author(String full_name, String location, int age, String details) {
        this.full_name = full_name;
        this.location = location;
        this.age = age;
        this.details = details;
    }
*/

}
