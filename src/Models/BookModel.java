/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

import Helpers.DbHelper;
import Models.Pojo.Author;
import Models.Pojo.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tejas
 */
public class BookModel {
    
    List<Book> books;
    private final String table_name = "books";

    public BookModel() {
    }
    
    public int save(Book book){
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        String query = "INSERT INTO `" + table_name + "`(`name`,`author_id`,`price`,`total_pages`) VALUES (?,?,?,?)";
        int inserted_flag = 0;
        PreparedStatement prepStmt = null;
        try {
            prepStmt = conn.prepareStatement(query);
            prepStmt.setString(1, book.getName());
            prepStmt.setInt(2, book.getAuthor_id());
            prepStmt.setFloat(3, book.getPrice());
            prepStmt.setInt(4, book.getTotal_pages());
                    
            inserted_flag = prepStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            dbHelper.closeAllConnections(conn, prepStmt, null);
        }
        return inserted_flag;
    }
    
    public List all(){
        
        books = new ArrayList<>();
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        ResultSet rs = null;
        String query = "SELECT * FROM "+table_name;
        Statement stmt = null;
        
        try { 
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Book book = setterForBook(rs);
                
                books.add(book);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("you are fuxckt");
        } finally {
                dbHelper.closeConnection(conn);
                dbHelper.closeStatement(stmt);
                dbHelper.closeResultSet(rs);               
        }
        return books;
        
    }
    
    public List whereName(String book_name){
        
        books = new ArrayList<>();
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        ResultSet rs = null;
        String query = "SELECT * FROM "+table_name+" WHERE name LIKE ?";
           
        PreparedStatement prepStmt = null ;
        try { 
            prepStmt = conn.prepareStatement(query);
            String book_name_query = book_name+"%";
            prepStmt.setString(1, book_name_query);
            rs = prepStmt.executeQuery();
            System.out.println("Book to search "+book_name);
            while (rs.next()) {
                Book book = setterForBook(rs);
                
                books.add(book);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("you are fuxckt");
        } finally {
                dbHelper.closeConnection(conn);
                dbHelper.closePreparedStmt(prepStmt);
                dbHelper.closeResultSet(rs);               
        }
        return books;
        
    }
    
    /*Private Functions Below : */
    
    private Book setterForBook(ResultSet rs) throws SQLException {
        Book book = new Book();
        book.setId(rs.getInt("id"));
        book.setName(rs.getString("name"));
        book.setAuthor_id(rs.getInt("author_id"));
        book.setPrice(rs.getFloat("price"));
        book.setTotal_pages(rs.getInt("total_pages"));
        //Set the author object for book.
        AuthorModel authorModel = new AuthorModel();
        Author author = authorModel.whereId(book.getAuthor_id());
        book.setAuthor(author);
        return book;
    }
}
