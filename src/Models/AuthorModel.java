/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

import Models.Pojo.Author;
import Helpers.DbHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tejas
 */
public class AuthorModel {
    
    List<Author> authors ;
    private final String table_name = "authors";

    public AuthorModel() {
    }
    
    public int save(Author author) {

        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        String query = "INSERT INTO `" + table_name + "`(`full_name`,`location`,`age`,`details`) VALUES (?,?,?,?)";
        int inserted_flag = 0;
        PreparedStatement prepStmt = null;
        try {
            prepStmt = conn.prepareStatement(query);
            prepStmt.setString(1, author.getFull_name());
            prepStmt.setString(2, author.getLocation());
            prepStmt.setInt(3, author.getAge());
            prepStmt.setString(4, author.getDetails());
            inserted_flag = prepStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            dbHelper.closeAllConnections(conn, prepStmt, null);
        }
        return inserted_flag;
    }
   
    public List all() {

        authors = new ArrayList<>();
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        ResultSet rs = null;
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            String query = "SELECT * FROM "+table_name;
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                Author author = setterForAuthor(rs); // call to private function
           
                authors.add(author);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("you are fuxckt");
        } finally {
                dbHelper.closeConnection(conn);
                dbHelper.closeStatement(stmt);
                dbHelper.closeResultSet(rs);               
        }
        return authors;
    }

   
    public List whereFullName(String authorName) {

        authors = new ArrayList<>();
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        ResultSet rs = null;
        String query = "SELECT * FROM "+table_name+" WHERE full_name LIKE ?";
           
        PreparedStatement prepStmt = null ;
        try { 
            prepStmt = conn.prepareStatement(query);
            String authorName_query = authorName+"%";
            prepStmt.setString(1, authorName_query);
            rs = prepStmt.executeQuery();
            
            while (rs.next()) {
                Author author = setterForAuthor(rs);    //call to private function
           
                authors.add(author);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("you are fuxckt");
        } finally {
                dbHelper.closeConnection(conn);
                dbHelper.closePreparedStmt(prepStmt);
                dbHelper.closeResultSet(rs);               
        }
        return authors;
    }
    
    public Author whereId(int author_id){
        
        Author author = new Author();
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        ResultSet rs = null;
        String query = "SELECT * FROM "+table_name+" WHERE id = ?";
           
        PreparedStatement prepStmt = null ;
        try { 
            prepStmt = conn.prepareStatement(query);
            
            prepStmt.setInt(1, author_id);
            rs = prepStmt.executeQuery();
            if(rs.first())
            {    
                 author = setterForAuthor(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("you are fuxckt");
        } finally {
                dbHelper.closeConnection(conn);
                dbHelper.closePreparedStmt(prepStmt);
                dbHelper.closeResultSet(rs);               
        }
        return author;
    }
    
    public int update(Author author){
        //how to update only changed fields ? 
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        String query = "UPDATE `authors` SET `full_name` = ?, `location` = ? , `age` = ? ,`details` = ? WHERE `id` = ?;";
        int inserted_flag = 0;
        PreparedStatement prepStmt = null;
        try {
            prepStmt = conn.prepareStatement(query);
            prepStmt.setString(1, author.getFull_name());
            prepStmt.setString(2, author.getLocation());
            prepStmt.setInt(3, author.getAge());
            prepStmt.setString(4, author.getDetails());
            prepStmt.setInt(5,author.getId());
            inserted_flag = prepStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Author.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            dbHelper.closeAllConnections(conn, prepStmt, null);
        }
        return inserted_flag;
        
    }
    
    /* Private Methods goes below : **/
    
     private Author setterForAuthor(ResultSet rs) throws SQLException {
        Author author = new Author();
        author.setId(rs.getInt("id"));
        author.setFull_name(rs.getString("full_name"));
        author.setLocation(rs.getString("location"));
        author.setAge(rs.getInt("age"));
        author.setDetails(rs.getString("details"));
        return author;
    }
    
}
