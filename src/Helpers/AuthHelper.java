/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tejas
 */
public class AuthHelper {

    /*USER_ID stores the authenticated users's id, This is accessible everywhere.
    This can be seen as an abstraction of Session ID */
    public static int USER_ID;
    Statement stmt;
    PreparedStatement prepStmt;
    ResultSet rs;

    public int attemptLogin(Connection conn, String username, String password) {

        try {

            String query = "SELECT count(*) as cnt FROM users WHERE email =? AND password =? ";

            prepStmt = conn.prepareStatement(query);
            prepStmt.setString(1, username);
            prepStmt.setString(2, password);
            rs = prepStmt.executeQuery();
            rs.first();

            USER_ID = rs.getInt(1);
            
            return USER_ID;

        } catch (SQLException ex) {
            Logger.getLogger(AuthHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DbHelper dbHelper = new DbHelper();
            dbHelper.closeAllConnections(conn, prepStmt, rs);
        }

        return 0;
    }

    public Map getAuthUser(Connection conn) {

        /* We want key-value pairs of user data, Hence HashMap*/
        Map<String, String> userData = new HashMap<>();

        String query = "SELECT * FROM users WHERE id=? ";
        try {
            prepStmt = conn.prepareStatement(query);
            prepStmt.setInt(1, USER_ID);
            rs = prepStmt.executeQuery();
            rs.first();
            userData.put("first_name", rs.getString(2));
            userData.put("last_name", rs.getString(3));
            userData.put("email", rs.getString(4));

        } catch (SQLException ex) {
            Logger.getLogger(AuthHelper.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("--Problem bro!");
        } finally {
            DbHelper dbHelper = new DbHelper();
            dbHelper.closeAllConnections(conn, prepStmt, rs);
        }
        return userData;
    }

}
