package Helpers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Tejas
 *
 * Nothing fancy here, Just checking If database is working.
 * To check just invoke the checkConnection method in Main Class.
 */
public class TestDBHelper {

    public void checkConnection() {

        Statement stmt = null;
        ResultSet rs = null ;
        DbHelper dbHelper = new DbHelper();
        Connection conn = dbHelper.getConnection();
        try {            
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM users");
            rs.first();
            System.out.println("--Reading first Row--");
            System.out.println("Id : " + rs.getInt(1));
            System.out.println("FirstName : " + rs.getString(2));
            System.out.println("LastName : " + rs.getString(3));
            System.out.println("It Just Works :) \n");

        } catch (SQLException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(TestDBHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

    }

}
