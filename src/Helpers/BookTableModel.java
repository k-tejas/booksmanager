/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Helpers;

import Models.Pojo.Author;
import Models.Pojo.Book;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Tejas
 */
public class BookTableModel extends AbstractTableModel{

    
    public String[] col_names = {"Id","Book Name","Author's Name","Price","Total Pages"};
    List list;
    
    public BookTableModel(List booksList) {
        this.list = booksList;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return col_names.length;
    }
    
    @Override
    public String getColumnName(int col){
        return col_names[col];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      Book book = (Book) list.get(rowIndex);
      
       Author author = book.getAuthor();
      
        switch(columnIndex){
            case 0 : return book.getId();
            case 1 : return book.getName();
            //case 2 : return book.getAuthor_id();
            case 2 : return author.getFull_name();
            case 3 : return book.getPrice();
            case 4 : return book.getTotal_pages();
                
        }
        return new String();
    }
    
    public Object getRowObject(int rowIndex){
        return (Book) list.get(rowIndex);
    }
}
