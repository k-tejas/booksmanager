/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tejas
 */
public class DbHelper {
    
   public Connection conn = null;
    
    public Connection getConnection() {
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost/db_first";
            conn = DriverManager.getConnection(url,"root","root");
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
       return conn;
        
    }
    
    public void closeConnection(Connection object){
       try {
           object.close();
       } catch (SQLException ex) {
           Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    public void closePreparedStmt(PreparedStatement object){
       try {
           object.close();
       } catch (SQLException ex) {
           Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    public void closeStatement(Statement object){
       try {
           object.close();
       } catch (SQLException ex) {
           Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    public void closeResultSet(ResultSet object){
       try {
           object.close();
       } catch (SQLException ex) {
           Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    public boolean closeAllConnections(Connection conn, PreparedStatement ps, ResultSet rs){
       try {
           if(rs != null){
           rs.close();
           }
           if(ps != null){
           ps.close();
           }
           conn.close();
       } catch (SQLException ex) {
           Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
       }
       finally{
           System.out.println("--Connection Closed --");
       }
        
        return true;
    }
    
       
}
