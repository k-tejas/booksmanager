/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Helpers;

import Models.Pojo.Author;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Tejas
 */
public class AuthorTableModel extends AbstractTableModel {

    public String[] col_names = {"Id","Full Name","Location","Age","Details"};
    
    List list;
    
    public AuthorTableModel(List authorList){
        this.list = authorList;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return col_names.length;
    }
    
    @Override
    public String getColumnName(int col){
        return col_names[col];
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Author author = (Author) list.get(rowIndex);
        
        switch(columnIndex){
            case 0 : return author.getId();
            case 1 : return author.getFull_name();
            case 2 : return author.getLocation();
            case 3 : return author.getAge();
            case 4 : return author.getDetails();
                
        }
        return new String();
    }
    
    public Author getRowObject(int rowIndex){
        return (Author) list.get(rowIndex);
    }
    
}
