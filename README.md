BookManager 
======
It's a practice app Created with NetBeans.

##Instructions to add Dependancies

Clone and Import this into your NetBeans.
Goto Properties of Project. > Libraries

Database.sql is included. 

Add Library : **MySQL Jdbc Driver**

Download [swingx-all-1.6.4 binary](https://java.net/projects/swingx/downloads/directory/releases)

Add Downloaded JAR : **swingx-all-1.6.4**